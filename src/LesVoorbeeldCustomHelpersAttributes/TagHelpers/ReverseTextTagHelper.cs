﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using MyHowest;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace LesVoorbeeldCustomHelpersAttributes.TagHelpers
{
    [HtmlTargetElement("reverse")]
    public class ReverseTextDivTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-for")]
        public string Keyword { get; set; }

        [HtmlAttributeName("asp-cap")]
        public bool Caps { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string result = Keyword.Reverse();
            if (Caps)
            {
                result = result.ToUpper();
            }

            output.TagName = "div";
            output.Attributes.Add("class", "text-danger");
            output.Content.SetContent(result);

            base.Process(context, output);
        }
    }
}

