﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LesVoorbeeldCustomHelpersAttributes.Models
{ 
    public enum Graad
    {
        Voldoende,
        Onderscheiding
    }
    public class Student
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public Graad Afstudeergraad { get; set; }

        public Student()
        {
        }

        public override string ToString()
        {
            return $"De student {Naam} heeft de graad {Afstudeergraad} behaald";
        }
    }
}
